/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * Copyright (C) 2019 Red Hat
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gio/gio.h>

#include "service-providers.h"

static void
test_positive_cb (const char *carrier,
                  const char *vvm_std,
                  const char *dest_num,
                  const char *carrier_prefix,
                  GError     *error,
                  gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_no_error (error);
  g_assert_cmpstr (carrier, ==, "Personal");
  g_assert_cmpstr (vvm_std, ==, "cvvm");
  g_assert_cmpstr (dest_num, ==, "098765");
  g_assert_cmpstr (carrier_prefix, ==, "//VVM");
}

static void
test_positive (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "345999",
                                        test_positive_cb,
                                        loop);

  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

static void
test_negative_cb (const char *carrier,
                  const char *vvm_std,
                  const char *dest_num,
                  const char *carrier_prefix,
                  GError     *error,
                  gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_error (error, 1, 1);
}

static void
test_negative (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "133666",
                                        test_negative_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

static void
test_nonexistent_cb (const char *carrier,
                     const char *vvm_std,
                     const char *dest_num,
                     const char *carrier_prefix,
                     GError     *error,
                     gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_assert_error (error, G_IO_ERROR, G_IO_ERROR_AGAIN);
  g_main_loop_quit (loop);
}

static void
test_nonexistent (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  vvmd_service_providers_find_settings ("nonexistent.xml",
                                        "13337",
                                        test_nonexistent_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/service-providers/positive", test_positive);
  g_test_add_func ("/service-providers/negative", test_negative);
  g_test_add_func ("/service-providers/nonexistent", test_nonexistent);

  return g_test_run ();
}
